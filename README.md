# Facial Expression Recognition

A real-time emotion recognition model that classifies seven different facial expressions

## Background
In recent years, automatic emotion recognition has gained much importance due to its numerous applications in the contemporary world: marketing, autonomous vehicle safety, human-robot interaction, and education are some examples. Researchers have utilized techniques ranging from machine learning and computer vision to signal processing and speech processing in order to approach this problem; however, emotion recognition remains a challenging task for computers, and arguably even humans at times. 

## Introduction
This project aims to classify the emotion on a person's face into one of seven categories using a deep 
convolutional neural network. The model was trained on the FER-2013 dataset, which consists of ~36k grayscale,
48x48 sized face images that fall into one of seven emotion cateogires: angry, disgusted, fearful, happy, neutral, sad, 
and surprised. In addition, the system uses the Haar Cascade Classifier to implement face detection, allowing the model to connect to a live webcam feed and generate emotion predictions in real-time.

## Dependencies
- Python 3
- TensorFlow (2.0.0)
- NumPy
- OpenCV

## Basic Usage

The repository is compatible with tensorflow-2.0 and makes use of the tensorflow.keras library.

## Authors and Acknowledgement

A special thank you to Dr. Tal Hassner for mentoring me throughout this project.

## Ongoing

If you are intersted in seeing a demo of the software or want more details about my development process, here is a link to my project blog: https://siliconvalley.basisindependent.com/author/ashnaa/. I will also include any future updates I have here!

## Reference
FER 2013 dataset curated by Pierre Luc Carrier and Aaron Courville, described in:

"Challenges in Representation Learning: A report on three machine learning contests," by Ian J. Goodfellow, 
Dumitru Erhan, Pierre Luc Carrier, Aaron Courville, Mehdi Mirza, Ben Hamner, Will Cukierski, Yichuan Tang, David Thaler, 
Dong-Hyun Lee, Yingbo Zhou, Chetan Ramaiah, Fangxiang Feng, Ruifan Li, Xiaojie Wang, Dimitris Athanasakis, John Shawe-Taylor, 
Maxim Milakov, John Park, Radu Ionescu, Marius Popescu, Cristian Grozea, James Bergstra, Jingjing Xie, Lukasz Romaszko, 
Bing Xu, Zhang Chuang, and Yoshua Bengio, arXiv:1307.0414.